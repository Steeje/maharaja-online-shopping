package com.maharaja.shopping.db;

import com.maharaja.shopping.db.common.Converters;
import com.maharaja.shopping.viewobject.AboutUs;
import com.maharaja.shopping.viewobject.Basket;
import com.maharaja.shopping.viewobject.Blog;
import com.maharaja.shopping.viewobject.Category;
import com.maharaja.shopping.viewobject.CategoryMap;
import com.maharaja.shopping.viewobject.City;
import com.maharaja.shopping.viewobject.Comment;
import com.maharaja.shopping.viewobject.CommentDetail;
import com.maharaja.shopping.viewobject.Country;
import com.maharaja.shopping.viewobject.DeletedObject;
import com.maharaja.shopping.viewobject.DiscountProduct;
import com.maharaja.shopping.viewobject.FavouriteProduct;
import com.maharaja.shopping.viewobject.FeaturedProduct;
import com.maharaja.shopping.viewobject.HistoryProduct;
import com.maharaja.shopping.viewobject.Image;
import com.maharaja.shopping.viewobject.LatestProduct;
import com.maharaja.shopping.viewobject.LikedProduct;
import com.maharaja.shopping.viewobject.Noti;
import com.maharaja.shopping.viewobject.PSAppInfo;
import com.maharaja.shopping.viewobject.PSAppVersion;
import com.maharaja.shopping.viewobject.Product;
import com.maharaja.shopping.viewobject.ProductAttributeDetail;
import com.maharaja.shopping.viewobject.ProductAttributeHeader;
import com.maharaja.shopping.viewobject.ProductCollection;
import com.maharaja.shopping.viewobject.ProductCollectionHeader;
import com.maharaja.shopping.viewobject.ProductColor;
import com.maharaja.shopping.viewobject.ProductListByCatId;
import com.maharaja.shopping.viewobject.ProductMap;
import com.maharaja.shopping.viewobject.ProductSpecs;
import com.maharaja.shopping.viewobject.Rating;
import com.maharaja.shopping.viewobject.RelatedProduct;
import com.maharaja.shopping.viewobject.ShippingMethod;
import com.maharaja.shopping.viewobject.Shop;
import com.maharaja.shopping.viewobject.ShopByTagId;
import com.maharaja.shopping.viewobject.ShopMap;
import com.maharaja.shopping.viewobject.ShopTag;
import com.maharaja.shopping.viewobject.SubCategory;
import com.maharaja.shopping.viewobject.TransactionDetail;
import com.maharaja.shopping.viewobject.TransactionObject;
import com.maharaja.shopping.viewobject.User;
import com.maharaja.shopping.viewobject.UserLogin;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


/**
 * Created by Panacea-Soft on 11/20/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Database(entities = {
        Image.class,
        Category.class,
        User.class,
        UserLogin.class,
        AboutUs.class,
        Product.class,
        LatestProduct.class,
        DiscountProduct.class,
        FeaturedProduct.class,
        SubCategory.class,
        ProductListByCatId.class,
        Comment.class,
        CommentDetail.class,
        ProductColor.class,
        ProductSpecs.class,
        RelatedProduct.class,
        FavouriteProduct.class,
        LikedProduct.class,
        ProductAttributeHeader.class,
        ProductAttributeDetail.class,
        Noti.class,
        TransactionObject.class,
        ProductCollectionHeader.class,
        ProductCollection.class,
        TransactionDetail.class,
        Basket.class,
        HistoryProduct.class,
        Shop.class,
        ShopTag.class,
        Blog.class,
        Rating.class,
        ShippingMethod.class,
        ShopByTagId.class,
        ProductMap.class,
        ShopMap.class,
        CategoryMap.class,
        PSAppInfo.class,
        PSAppVersion.class,
        DeletedObject.class,
        Country.class,
        City.class

}, version = 7, exportSchema = false)
//V2.4 = DBV 7
//V2.3 = DBV 7
//V2.2 = DBV 7
//V2.1 = DBV 7
//V2.0 = DBV 7
//V1.9 = DBV 7
//V1.8 = DBV 7
//V1.7 = DBV 6
//V1.6 = DBV 5
//V1.5 = DBV 4
//V1.4 = DBV 3
//V1.3 = DBV 2


@TypeConverters({Converters.class})

public abstract class PSCoreDb extends RoomDatabase {

    abstract public UserDao userDao();

    abstract public ProductColorDao productColorDao();

    abstract public ProductSpecsDao productSpecsDao();

    abstract public ProductAttributeHeaderDao productAttributeHeaderDao();

    abstract public ProductAttributeDetailDao productAttributeDetailDao();

    abstract public BasketDao basketDao();

    abstract public HistoryDao historyDao();

    abstract public AboutUsDao aboutUsDao();

    abstract public ImageDao imageDao();

    abstract public CountryDao countryDao();

    abstract public CityDao cityDao();

    abstract public RatingDao ratingDao();

    abstract public CommentDao commentDao();

    abstract public CommentDetailDao commentDetailDao();

    abstract public ProductDao productDao();

    abstract public CategoryDao categoryDao();

    abstract public SubCategoryDao subCategoryDao();

    abstract public NotificationDao notificationDao();

    abstract public ProductCollectionDao productCollectionDao();

    abstract public TransactionDao transactionDao();

    abstract public TransactionOrderDao transactionOrderDao();

    abstract public ShopDao shopDao();

    abstract public BlogDao blogDao();

    abstract public ShippingMethodDao shippingMethodDao();

    abstract public ProductMapDao productMapDao();

    abstract public CategoryMapDao categoryMapDao();

    abstract public PSAppInfoDao psAppInfoDao();

    abstract public PSAppVersionDao psAppVersionDao();

    abstract public DeletedObjectDao deletedObjectDao();


//    /**
//     * Migrate from:
//     * version 1 - using Room
//     * to
//     * version 2 - using Room where the {@link } has an extra field: addedDateStr
//     */
//    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE news "
//                    + " ADD COLUMN addedDateStr INTEGER NOT NULL DEFAULT 0");
//        }
//    };

    /* More migration write here */
}